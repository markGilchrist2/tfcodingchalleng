package com.gameforeverything.tflcodingchallenge.repository

import com.gameforeverything.tflcodingchallenge.model.Success
import io.reactivex.Single

interface NetworkRepository {
    fun getRoadStatus(road: String): Single<Success>
}