package com.gameforeverything.tflcodingchallenge.repository

import com.gameforeverything.tflcodingchallenge.model.ObjectConverter
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class FuelNetworkRepositoryImplTest {

    @Mock
    private lateinit var converter: ObjectConverter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun createUrl() {
        // arrange
        val impl = FuelNetworkRepositoryImpl(converter, "3ba46adf","582a994c8d41a820c9fc512e931861f1")

        // act
        val url = impl.createUrl("a2")

        // assert
        assertEquals("https://api.tfl.gov.uk/Road/a2?app_id=3ba46adf&app_key=582a994c8d41a820c9fc512e931861f1",url)
    }
}