package com.gameforeverything.tflcodingchallenge.model

import kotlinx.serialization.Serializable

sealed class ServerResponse

@Serializable
data class Success(
    val id: String,
    val displayName: String,
    val statusSeverity: String,
    val statusSeverityDescription: String,
    val url: String
) : ServerResponse()


@Serializable
data class Failure(
    val message: String
) : ServerResponse()