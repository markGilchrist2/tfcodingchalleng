package com.gameforeverything.tflcodingchallenge.repository

import com.gameforeverything.tflcodingchallenge.model.ObjectConverter
import com.gameforeverything.tflcodingchallenge.model.Success
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.rx.rxResponseString
import io.reactivex.Single

class FuelNetworkRepositoryImpl(
    private val converter: ObjectConverter,
    private val appId: String = "3ba46adf",
    private val appKey: String = "582a994c8d41a820c9fc512e931861f1"
) : NetworkRepository {

    override fun getRoadStatus(road: String): Single<Success> =
        createUrl(road)
            .httpGet()
            .rxResponseString()
            .map { converter.convert(it) }

    fun createUrl(road: String) =
        "https://api.tfl.gov.uk/Road/$road?app_id=$appId&app_key=$appKey"
}