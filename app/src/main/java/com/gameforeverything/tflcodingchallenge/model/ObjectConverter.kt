package com.gameforeverything.tflcodingchallenge.model

interface ObjectConverter {
    fun convert(json: String) : Success
}