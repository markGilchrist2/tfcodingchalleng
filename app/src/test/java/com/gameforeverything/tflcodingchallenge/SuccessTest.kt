package com.gameforeverything.tflcodingchallenge

import com.gameforeverything.tflcodingchallenge.model.Failure
import com.gameforeverything.tflcodingchallenge.model.Success
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import org.junit.Assert.assertEquals
import org.junit.Test

class SuccessTest {

    @Test
    fun `ensure that sample json passes to object`() {
        // arrange act
        val actual = Json(JsonConfiguration.Stable.copy(ignoreUnknownKeys = true)).parse(
            Success.serializer(),
            successJson
        )

        // assert
        assertEquals(actual.id, "a2")
        assertEquals(actual.displayName, "A2")
        assertEquals(actual.statusSeverityDescription, "No Exceptional Delays")
    }

    @Test
    fun `ensure failure passes `(){
        // arrange act
        val actual = Json(JsonConfiguration.Stable.copy(ignoreUnknownKeys = true)).parse(
            Failure.serializer(),
            failureJson
        )

        // assert
        assertEquals("The following road id is not recognised: A233",actual.message)
    }

    private val failureJson = """
        {
          "timestampUtc": "2017-11-21T14:37:39.7206118Z",
          "exceptionType": "EntityNotFoundException",
          "httpStatusCode": 404,
          "httpStatus": "NotFound",
          "relativeUri": "/Road/A233",
          "message": "The following road id is not recognised: A233"
        }
    """.trimIndent()

    private val successJson = """{
        "id": "a2",
        "displayName": "A2",
        "statusSeverity": "Good",
        "statusSeverityDescription": "No Exceptional Delays",
        "bounds": "[[-0.0857,51.44091],[0.17118,51.49438]]",
        "envelope": "[[-0.0857,51.44091],[-0.0857,51.49438],[0.17118,51.49438],[0.17118,51.44091],[-0.0857,51.44091]]",
        "url": "/Road/a2"
    }""".trimIndent()
}