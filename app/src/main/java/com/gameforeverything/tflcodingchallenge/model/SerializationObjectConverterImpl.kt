package com.gameforeverything.tflcodingchallenge.model

import kotlinx.serialization.builtins.list
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

class SerializationObjectConverterImpl:
    ObjectConverter {
    override fun convert(json: String): Success =
        Json(JsonConfiguration.Stable.copy(ignoreUnknownKeys = true)).parse(Success.serializer().list, json)[0]
}