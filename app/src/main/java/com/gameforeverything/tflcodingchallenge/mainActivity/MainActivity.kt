package com.gameforeverything.tflcodingchallenge.mainActivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.gameforeverything.tflcodingchallenge.R
import com.gameforeverything.tflcodingchallenge.model.SerializationObjectConverterImpl
import com.gameforeverything.tflcodingchallenge.repository.FuelNetworkRepositoryImpl
import com.gameforeverything.tflcodingchallenge.repository.NetworkRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val repository: NetworkRepository = FuelNetworkRepositoryImpl(
        SerializationObjectConverterImpl()
    )
    private val viewModel = MainActivityViewModel(repository)
    private val bag = CompositeDisposable()
    private val textSubject = PublishSubject.create<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        bag.add(viewModel.statusSeverityDescription
            .subscribe { roadStatusDescription.text = it }
        )
        bag.add(viewModel.statusSeverity
            .subscribe { roadStatusLbl.text = it }
        )
        viewModel.bindTextChange(textSubject)
        enterRoadEt.addTextChangedListener { editable ->
            editable?.toString()?.let {
                textSubject.onNext(it)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        bag.clear()
    }
}
