package com.gameforeverything.tflcodingchallenge.mainActivity

import com.gameforeverything.tflcodingchallenge.model.Success
import com.gameforeverything.tflcodingchallenge.repository.NetworkRepository
import com.github.kittinunf.fuel.core.FuelError
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MainActivityViewModelTest {

    @Mock
    private lateinit var repository: NetworkRepository

    private lateinit var viewModel: MainActivityViewModel

    private val textSubject = PublishSubject.create<String>()

    private val validRoad = "a2"
    private val invalidRoad = "aun222"

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = MainActivityViewModel(repository)
        viewModel.bindTextChange(textSubject)
        whenever(repository.getRoadStatus(validRoad)).then { Single.just(success) }
        whenever(repository.getRoadStatus(invalidRoad)).then {
            Observable.error<FuelError> {
                FuelError.wrap(
                    Exception()
                )
            }
        }
    }

    @Test
    fun `ensure that on success then fail text is displayed and clears (statusSeverity)`() {
        // arrange
        val observer = TestObserver<String>()
        viewModel.statusSeverity.subscribe(observer)

        // act
        textSubject.onNext(invalidRoad)
        textSubject.onNext(validRoad)

        // assert
        observer.assertNotTerminated()
        observer.assertValues("", "not sever")
    }

    @Test
    fun `ensure that on success then fail text is displayed and clears (statusSeverityDescription)`() {
        // arrange
        val observer = TestObserver<String>()
        viewModel.statusSeverityDescription.subscribe(observer)

        // act
        textSubject.onNext(invalidRoad)
        textSubject.onNext(validRoad)

        // assert
        observer.assertNotTerminated()
        observer.assertValues("", "really not sever")
    }

    private val success = Success(
        "id",
        "test name",
        "not sever",
        "really not sever",
        "checkIt/road"
    )
}