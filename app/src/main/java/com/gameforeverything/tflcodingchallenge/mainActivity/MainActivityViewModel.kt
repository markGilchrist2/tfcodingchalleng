package com.gameforeverything.tflcodingchallenge.mainActivity

import com.gameforeverything.tflcodingchallenge.model.Failure
import com.gameforeverything.tflcodingchallenge.model.ServerResponse
import com.gameforeverything.tflcodingchallenge.model.Success
import com.gameforeverything.tflcodingchallenge.repository.NetworkRepository
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

class MainActivityViewModel(
    private val repository: NetworkRepository
) {
    private val subject = BehaviorSubject.create<ServerResponse>()
    private val bag = CompositeDisposable()

    val statusSeverityDescription: Observable<String> = subject.map {
        when (it) {
            is Success -> it.statusSeverityDescription
            is Failure -> ""
        }
    }

    val statusSeverity: Observable<String> = subject.map {
        when (it) {
            is Success -> it.statusSeverity
            is Failure -> ""
        }
    }

    //
    fun bindTextChange(strings: Observable<String>) {
        bag.add(
            strings
                .filter { it.length >= 2 } // roads must have at least on number and letter
                //.filter { todo add client side validation here for better user experience }
                .flatMap { repository.getRoadStatus(it).toObservable() }
                .map { it }
                .doOnError { subject.onNext(
                    Failure(
                        "Road not found"
                    )
                ) }
                .retry()
                .subscribe { subject.onNext(it) }
        )
    }
}